/*
 * Renderer.hpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#ifndef RENDERER_HPP_
#define RENDERER_HPP_

#include <memory>
#include "Scene.hpp"
#include "Ray.hpp"
#include "glutwindow.hpp"
#include "ppmwriter.hpp"

class Renderer {

	public:

		Renderer( Scene const & scene );
		~Renderer();

		// raytracing is initiated from here
		void render();

	private : // attributes
		Scene scene;

};

#endif /* RENDERER_HPP_ */
