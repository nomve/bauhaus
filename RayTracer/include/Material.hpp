/*
 * Material.hpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */

#include <string>
#include "color.hpp"
#include <iostream>

#ifndef MATERIAL_HPP_
#define MATERIAL_HPP_

class Material {
	public:
		Material(std::string const & name, Color const & ka, Color const & kd, Color const & ks, float m);
		~Material();

		std::string get_name() const;
		Color get_ka() const;
		Color get_kd() const;
		Color get_ks() const;
		double get_m() const;

	private:
		std::string name;
		Color ka, kd, ks;
		double m;
};

#endif /* MATERIAL_HPP_ */
