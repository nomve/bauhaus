/*
 * SDF.hpp
 *
 *  Created on: Aug 2, 2013
 *      Author: nomve
 */


#ifndef SDF_HPP_
#define SDF_HPP_

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <fstream>
#include "Scene.hpp"
#include "Material.hpp"
#include "Object.hpp"
#include "Light.hpp"
#include "Camera.hpp"

class SDF {

	public:
		SDF();
		SDF( std::string filename );
		~SDF();

		void set_file( std::string name );
		void open_file();
		void parse_scene( Scene & scene );

	private:
		std::string filename;
		std::ifstream file;



};

#endif /* SDF_HPP_ */
