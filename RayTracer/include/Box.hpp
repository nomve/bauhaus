/*
 * Box.hpp
 *
 *  Created on: Aug 2, 2013
 *      Author: nomve
 */

#ifndef BOX_HPP_
#define BOX_HPP_

#include "Object.hpp"
#include <string>
#include <memory>
#include "point.hpp"
#include "vector.hpp"

class Box: public Object {
	public:
		Box( std::string name, std::shared_ptr<Material> material, math3d::point const & p1, math3d::point const & p2 );
		~Box();

		double intersects( Ray const & ray ) const;
		math3d::vector calculate_normal( math3d::point const & );

		math3d::point get_min_point() const;
		math3d::point get_max_point() const;


	private:
		math3d::point min, max;
};

#endif /* BOX_HPP_ */
