/*
 * Camera.hpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <string>
#include "point.hpp"
#include "Ray.hpp"

class Camera {
	public:
		Camera( std::string name, double fov_x );
		~Camera();

		std::string get_name() const;
		double get_z() const;
		math3d::point get_position() const;
		//shooting rays through the 'screen'
		Ray fire_ray( double, double, double, double ) const;

	private:
		std::string name;
		double fov_x;
		math3d::point position;
		/*
		 * distance to the 'screen'
		 * proportional to 'screen' width ( which is 1 )
		 * is the z value when firing rays
		 */
		double distance;
};

#endif /* CAMERA_HPP_ */
