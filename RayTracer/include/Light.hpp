/*
 * Light.hpp
 *
 *  Created on: Aug 2, 2013
 *      Author: nomve
 */

#ifndef LIGHT_HPP_
#define LIGHT_HPP_

#include <string>
#include "point.hpp"
#include "color.hpp"

class Light {
	public:
		Light( std::string name, math3d::point const & position, Color const & la, Color const & ld );
		~Light();

		math3d::point get_position() const;

		Color get_la() const;
		Color get_ld() const;

	private:
		std::string name;
		math3d::point position;
		Color la, ld;
};

#endif /* LIGHT_HPP_ */
