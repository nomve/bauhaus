/*
 * Object.hpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */


#ifndef OBJECT_HPP_
#define OBJECT_HPP_

#include <string>
#include <memory>

#include "Material.hpp"
#include "Ray.hpp"
#include "vector.hpp"
#include "point.hpp"
#include "matrix.hpp"

class Object {

	public:
		Object( std::string name, std::shared_ptr<Material> material, bool is_transformed = false );
		virtual ~Object();

		std::string get_name() const;
		std::shared_ptr<Material> get_material() const;

		virtual double intersects( Ray const & ray ) const = 0;
		virtual math3d::vector calculate_normal( math3d::point const & ) = 0;

		bool transformed() const;
		void set_transformation( math3d::matrix & );

		math3d::matrix transformation() const;
		math3d::matrix transformation_inv() const;

	protected:
		std::string name;
		std::shared_ptr<Material> material;
		bool is_transformed;
		math3d::matrix world_transformation;
		math3d::matrix world_transformation_inv;

};

#endif /* OBJECT_HPP_ */
