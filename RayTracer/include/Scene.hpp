/*
 * Scene.hpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#ifndef SCENE_HPP_
#define SCENE_HPP_

#include <vector>
#include <iostream>
#include <memory>
#include <map>
#include <string>
#include "vector.hpp"
#include "Object.hpp"
#include "Material.hpp"
#include "Camera.hpp"
#include "Light.hpp"
#include "Intersection.hpp"

class Scene {
	public:

		Scene();
		Scene( std::string scene_name );
		~Scene();

		void insert_material( std::shared_ptr<Material> );
		std::shared_ptr<Material> find_material( std::string const & ) const;

		void insert_object( std::shared_ptr<Object> );
		std::shared_ptr<Object> find_object( std::string const & ) const ;
		//returns ray direction parameter (point location)
		Intersection closest_object( Ray & );
		Intersection retrace( Ray const &, std::shared_ptr<Object> );
		math3d::vector reflect( math3d::vector const &, math3d::vector const & );

		void insert_light( std::shared_ptr<Light> );

		void insert_camera( std::shared_ptr<Camera> );
		std::shared_ptr<Camera> find_camera( std::string const & ) const;
		void set_used_camera( std::shared_ptr<Camera> );
		std::shared_ptr<Camera> get_used_camera() const;

		void set_scene_details( std::string name, std::size_t width, std::size_t height);

		Color point_color( Intersection const &, int hop = 1, int max = 3 );

		std::string get_name() const;
		std::string get_filename() const;
		std::size_t get_width() const;
		std::size_t get_height() const;

	private:
		std::string file_name;
		std::string scene_name;
		std::size_t scene_width;
		std::size_t scene_height;
		std::shared_ptr<Camera> camera_used;
		//all objects (shapes)
		std::vector< std::shared_ptr<Object> > objects;
		//lights
		std::vector< std::shared_ptr<Light> > lights;
		//camera
		std::map< std::string, std::shared_ptr<Camera> > cameras;
		//action ... errr materials
		std::map< std::string, std::shared_ptr<Material> > materials;
};

#endif /* SCENE_HPP_ */
