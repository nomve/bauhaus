/*
 * Intersection.hpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#ifndef INTERSECTION_HPP_
#define INTERSECTION_HPP_

#include <memory>
#include "Object.hpp"
#include "point.hpp"

class Intersection {

	public:
	Intersection( bool = false );
	Intersection( bool, std::shared_ptr<Object>, math3d::point const & );
	~Intersection();

	std::shared_ptr<Object> get_object() const;
	math3d::point get_point() const;
	bool exists() const;

	private:
		bool intersection;
		math3d::point point;
		std::shared_ptr<Object> object;
};

#endif /* INTERSECTION_HPP_ */
