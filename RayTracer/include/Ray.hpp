/*
 * Ray.hpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */

#ifndef RAY_HPP_
#define RAY_HPP_

#include "point.hpp"
#include "vector.hpp"
#include "matrix.hpp"

class Ray {
	public:
		Ray( math3d::point const & origin, math3d::vector const & direction, double param = 1.0 );
		~Ray();

		math3d::vector get_direction() const;
		math3d::point get_origin() const;

		//used for inverse transformations
		void transform( math3d::matrix const & );

	private:
		math3d::point origin;
		math3d::vector direction;
		//direction vector parameter
		double param;
};

#endif /* RAY_HPP_ */
