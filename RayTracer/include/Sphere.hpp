/*
 * Sphere.hpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */

#ifndef SPHERE_HPP_
#define SPHERE_HPP_

#include <memory>
#include <string>

#include "point.hpp"
#include "vector.hpp"
#include "Object.hpp"
#include "Ray.hpp"

class Sphere: public Object {
	public:
		Sphere( std::string name, std::shared_ptr<Material> material, double radius, math3d::point const & center );
		~Sphere();

		double intersects( Ray const & ray ) const;
		math3d::vector calculate_normal( math3d::point const & );

		math3d::point get_center() const;
		double get_radius() const;

	private:
		double radius;
		math3d::point center;
};

#endif /* SPHERE_HPP_ */
