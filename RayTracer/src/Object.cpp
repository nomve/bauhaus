/*
 * Object.cpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */

#include "Object.hpp"

Object::Object( std::string name, std::shared_ptr<Material> material, bool is_transformed )
	: name(name), material(material), is_transformed(is_transformed) {}

Object::~Object() {}

std::string Object::get_name() const {
	return name;
}

std::shared_ptr<Material> Object::get_material() const {
	return material;
}

bool Object::transformed() const {
	return is_transformed;
}

void Object::set_transformation( math3d::matrix & transform ) {

	world_transformation *= transform;

	math3d::matrix temp = world_transformation;

	if ( temp.invert() )
		world_transformation_inv = temp;

	is_transformed = true;

}

math3d::matrix Object::transformation() const {
	return world_transformation;
}
math3d::matrix Object::transformation_inv() const {
	return world_transformation_inv;
}
