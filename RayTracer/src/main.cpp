#include <glutwindow.hpp>
#include <ppmwriter.hpp>
#include <pixel.hpp>

#include <iostream>
#include <cmath>

#include <thread>
#include <functional>

#include "Renderer.hpp"
#include "SDF.hpp"

#ifdef __APPLE__
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#ifndef  M_PI
#define M_PI		3.14159265358979323846
#endif

int main(int argc, char* argv[])
{

	//create empty scene
	Scene scene("my scene");

	//parse the SDF file
	SDF parser("scene");
	parser.parse_scene(scene);

	// create output window
	glutwindow::init(scene.get_width(), scene.get_height(), 100, 100, scene.get_name(), argc, argv);

	// create a ray tracing Renderer
	Renderer app(scene) /* (scene) */;
	//set renderer options

	// start computation in thread
	std::thread thr(std::bind(&Renderer::render, &app));

	// start output on glutwindow
	glutwindow::instance().run();

	// wait on thread
	thr.join();

	//example_math3d();

	return 0;
}
