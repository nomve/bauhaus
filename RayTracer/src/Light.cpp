/*
 * Light.cpp
 *
 *  Created on: Aug 2, 2013
 *      Author: nomve
 */

#include "Light.hpp"

Light::Light( std::string name, math3d::point const & position, Color const & la, Color const & ld )
	: name(name), position(position), la(la), ld(ld) {}

Light::~Light() {}

math3d::point Light::get_position() const {
	return position;
}

Color Light::get_la() const {
	return la;
}

Color Light::get_ld() const {
	return ld;
}
