/*
 * Sphere.cpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */

#include "Sphere.hpp"
#include "vector.hpp"
#include <cmath>
#include <limits>

Sphere::Sphere( std::string name, std::shared_ptr<Material> material, double radius, math3d::point const & center )
	: Object(name, material),
	  radius(radius), center(center) {};

Sphere::~Sphere() {}

double Sphere::intersects( Ray const & ray ) const {

	/*
	 * digest of the math
	 * http://wiki.cgsociety.org/index.php/Ray_Sphere_Intersection
	 */
	math3d::point ray_origin = ray.get_origin();
	math3d::vector ray_direction = ray.get_direction();
	math3d::point sphere_center = this->get_center();
	math3d::vector difference = ray_origin - sphere_center;

	double a = math3d::dot( ray_direction, ray_direction );
	double b = 2*math3d::dot( difference, ray_direction );
	double c = math3d::dot( difference, difference ) - std::pow( this->get_radius(), 2);



	double d = std::pow(b,2) - 4*a*c;
	//std::cout << d << std::endl;
	if ( d < 0 )
	{
		return 0;
	}

	d = std::sqrt(d);
	a = 2*a;

	double param1 = (-b-d)/a;
	double param2 = (-b+d)/a;

	//too small
	//double limit = std::numeric_limits<double>::min();
	//double limit = std::numeric_limits<double>::epsilon();
	double limit = 1e-10;

	/*
	 * the ray tested is a vector in parametric form
	 * with parameter = 1
	 * if the found parameter is smaller than 0
	 * the intersecting objects is behind the starting point
	 * and not interesting
	 * if it's zero or close to it, it's exactly the starting point
	 * which we also don't want, when testing if reflecting rays reach the camera
	 * 1e-10 is arbitrary and the method can probably be improved
	 */

	if ( param1 < limit && param2 < limit )
		return 0;
	else if ( param2 < param1 || param1 < limit )
		return param2;
	else if ( param1 < param2 || param2 < limit )
		return param1;

	return param1;

}

math3d::vector Sphere::calculate_normal( math3d::point const & point ) {

	math3d::vector normal = point - this->get_center();

	return normal;

}

math3d::point Sphere::get_center() const {
	return center;
}

double Sphere::get_radius() const {
	return radius;
}
