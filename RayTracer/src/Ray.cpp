/*
 * Ray.cpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */

#include "Ray.hpp"

Ray::Ray( math3d::point const & origin, math3d::vector const & direction, double param )
	: origin(origin), direction(direction), param(param) {}

Ray::~Ray() {}

math3d::vector Ray::get_direction() const {
	return direction;
}

math3d::point Ray::get_origin() const {
	return origin;
}

void Ray::transform ( math3d::matrix const & transform ) {

	direction = transform * direction;

}
