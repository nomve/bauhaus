/*
 * Intersection.cpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#include "Intersection.hpp"

Intersection::Intersection(bool exists)
	: intersection(exists) {}

Intersection::Intersection(bool exists, std::shared_ptr<Object> object, math3d::point const & point )
	: intersection(exists), object(object), point(point)  {}

Intersection::~Intersection() {}

bool Intersection::exists() const{
	return intersection;
}

std::shared_ptr<Object> Intersection::get_object() const {
	return object;
}

math3d::point Intersection::get_point() const {
	return point;
}
