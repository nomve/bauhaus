/*
 * Renderer.cpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#include "Renderer.hpp"
#include "Intersection.hpp"
#include "time.h"

Renderer::Renderer( Scene const & scene )
	: scene(scene) {}

Renderer::~Renderer() {}

void Renderer::render() {
	// get glutwindow instance
	glutwindow& window = glutwindow::instance();

	// create a ppmwriter
	ppmwriter image (window.width(), window.height(), "./" + scene.get_filename() );

	//get camera
	auto camera = scene.get_used_camera();

	// for all pixels of window
	for (std::size_t y = 0; y < window.height(); ++y) {
		  for (std::size_t x = 0; x < window.width(); ++x) {

				Pixel p(x, y);

				/*
				 * test ray
				 */
				Ray ray = camera->fire_ray( x, y, window.width(), window.height() );

				//std::cout << ray.get_direction() << std::endl;

				Intersection result = scene.closest_object( ray );

				if ( result.exists() )
					p.color = scene.point_color( result );
				else
					//p.color = Color(float(y)/(window.height()*5),float(y)/(window.height()*8),float(x)/(window.width()*10));
					p.color = Color(0,0,0);

				window.write(p); // write pixel to output window
				image.write(p);  // write pixel to image writer
		  }
	}

	// save final image
	image.save();
}
