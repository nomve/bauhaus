/*
 * Scene.cpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#include "Scene.hpp"
#include "point.hpp";
#include "vector.hpp"
#include "matrix.hpp"
#include <limits>
#include <memory>
#include <cmath>

Scene::Scene() {}

Scene::Scene( std::string scene_name )
	: scene_name(scene_name) {}

Scene::~Scene() {}

void Scene::insert_material( std::shared_ptr<Material> material ) {

	materials.insert(
		std::make_pair(
			material->get_name(), material
		)
	);
}

std::shared_ptr<Material> Scene::find_material( std::string const & name ) const {
	return materials.find(name)->second;
}

void Scene::insert_object( std::shared_ptr<Object> object ) {
	objects.push_back( object );
}

std::shared_ptr<Object> Scene::find_object( std::string const & name ) const {
	//well...
	//I should've used a map
	for ( auto i = objects.begin(); i != objects.end(); ++i )
	{
		if ( name == (*i)->get_name() )
			return *i;
	}
}

Intersection Scene::closest_object( Ray & ray ) {

	std::shared_ptr<Object> object;
	double param = std::numeric_limits<double>::infinity();
	bool found = false;

	for ( auto it = objects.begin(); it != objects.end(); ++it )
	{
		//current object
		auto current = *it;

		/*
		 * check if object is transformed
		 * and do an inverse transformation of the ray
		 */
		if ( current->transformed() )
			ray.transform( current->transformation_inv() );

		double temp = current->intersects(ray);
		if ( temp != 0 && temp < param )
		{
			found = true;
			//save distance and object
			param = temp;
			object = current;
		}
	}

	if ( ! found )
		return Intersection( false );

	/*
	 * intersection point from the parametric form
	 */
	math3d::point point = ray.get_origin() + ( param * ray.get_direction() );

	return Intersection( true, object, point );

}

void Scene::insert_light( std::shared_ptr<Light> light ) {
	lights.push_back( light );
}

void Scene::insert_camera( std::shared_ptr<Camera> camera ) {

	cameras.insert(
		std::make_pair(
			camera->get_name(), camera
		)
	);
}

std::shared_ptr<Camera> Scene::find_camera( std::string const & name ) const {
	return cameras.find(name)->second;
}

void Scene::set_used_camera( std::shared_ptr<Camera> camera ) {
	camera_used = camera;
}

std::shared_ptr<Camera> Scene::get_used_camera() const {
	return camera_used;
}

void Scene::set_scene_details( std::string name, std::size_t width, std::size_t height )  {
	file_name = name;
	scene_width = width;
	scene_height = height;
}

std::string Scene::get_name() const {
	return scene_name;
}

std::string Scene::get_filename() const {
	return file_name;
}

std::size_t Scene::get_width() const {
	return scene_width;
}

std::size_t Scene::get_height() const {
	return scene_height;
}

Color Scene::point_color( Intersection const & intersection, int hop, int max ) {

	/*
	 * if called recursive, don't advance past the max hops
	 */
	if ( hop > max )
		//nothing there
		return Color(0,0,0);

	/*
	 * intersecting object
	 */
	auto object = intersection.get_object();
	auto material = object->get_material();

	/*
	 * current color with ambient light
	 */
	Color point_color;
	//other colors
	Color ambient;
	Color diffuse;
	Color reflecting;

	/*
	 * intersecting point
	 */
	math3d::point point = intersection.get_point();
	if ( object->transformed() )
		point = object->transformation() * point;

	/*
	 * normal vector to the object in the intersection point
	 */
	math3d::vector normal = object->calculate_normal( point );
	if ( object->transformed() )
	{
		math3d::matrix temp = object->transformation_inv();
		temp.transpose();
		normal = temp * normal;
	}

	/*
	 * check light sources
	 */
	for ( auto i = lights.begin(); i != lights.end(); ++i )
	{
		//set ambient light
		ambient += material->get_ka() * (*i)->get_la();
		//position of the light
		auto light_position = (*i)->get_position();
		//direction vector from the point to the light source
		math3d::vector to_light_direction = light_position - point;
		//construct ray from point to light source
		Ray to_light( point, to_light_direction );
		//check for intersecting objects
		Intersection object_result = this->closest_object( to_light );
		//there are no objects blocking this light source
		if ( ! object_result.exists() )
		{
			//diffuse color
			normal.normalize();
			to_light_direction.normalize();
			double product = math3d::dot( normal, to_light_direction );
			//multiply material's diffuse color with the dot product
			diffuse = material->get_kd() * product;

			//vector to camera
			math3d::point camera_position = this->get_used_camera()->get_position();
			math3d::vector to_camera_direction = camera_position - point;

			to_camera_direction.normalize();
			//reflecting
			math3d::vector reflecting_direction = this->reflect( to_light_direction, normal );
			//cosinus between the ray to camera
			//and the reflecting direction
			double angle_cos = math3d::dot( to_camera_direction, reflecting_direction );
			double reflecting_factor = std::pow( angle_cos, material->get_m() );
			//set reflecting color
			reflecting = material->get_ks() * reflecting_factor;

			//reflecting ray
			Ray reflecting_ray( point, reflecting_direction );
			Intersection reflecting_result = this->closest_object( reflecting_ray );
			/*
			 * see who's in the way
			 */
			if ( reflecting_result.exists() )
			{
				/*
				 * don't do anything if the blocking object is actually the same
				 * with the point being the other side of the object
				 */

				auto reflecting_object = reflecting_result.get_object();

				if ( reflecting_object != object )
					//call the method again
					point_color += this->point_color( reflecting_result, ++hop ) * (
							material->get_ks()
							* (material->get_m()/100));
			}

			//total
			point_color +=  (*i)->get_ld() * (diffuse+reflecting);

		}


	}

	return ambient+point_color;

}

math3d::vector Scene::reflect( math3d::vector const & source, math3d::vector const & normal ) {

	return -source - 2*normal*math3d::dot( -source, normal );

}
