/*
 * Box.cpp
 *
 *  Created on: Aug 2, 2013
 *      Author: nomve
 */

#include "Box.hpp"
#include <limits>
#include <cmath>

Box::Box( std::string name, std::shared_ptr<Material> material, math3d::point const & min, math3d::point const & max )
	: Object( name, material ), min(min), max(max) {}

Box::~Box() {}

math3d::point Box::get_min_point() const {
	return min;
}

math3d::point Box::get_max_point() const {
	return max;
}

double Box::intersects( Ray const & ray ) const {

	math3d::point ray_origin = ray.get_origin();
	math3d::vector ray_direction = ray.get_direction();

	double pmin, pmax, pymin, pymax, pzmin, pzmax = 0;

	if ( ray_direction[0] >= 0 )
	{
		pmin = ( min[0] - ray_origin[0] ) / ray_direction[0];
		pmax = ( max[0] - ray_origin[0] ) / ray_direction[0];
	}
	else
	{
		pmax = ( min[0] - ray_origin[0] ) / ray_direction[0];
		pmin = ( max[0] - ray_origin[0] ) / ray_direction[0];
	}


	if ( ray_direction[1] >= 0 )
	{
		pymin = ( min[1] - ray_origin[1] ) / ray_direction[1];
		pymax = ( max[1] - ray_origin[1] ) / ray_direction[1];
	}
	else
	{
		pymax = ( min[1] - ray_origin[1] ) / ray_direction[1];
		pymin = ( max[1] - ray_origin[1] ) / ray_direction[1];
	}

	if ( pmin > pymax || pymin > pmax )
	{
		return 0;
	}

	if ( pymin > pmin )
		pmin = pymin;

	if ( pymax < pmax )
		pmax = pymax;

	//our view direction is the negative z achsis
	//so negative is actually positive :)
	if ( ray_direction[2] <= 0 )
	{
		pzmin = ( min[2] - ray_origin[2] ) / ray_direction[2];
		pzmax = ( max[2] - ray_origin[2] ) / ray_direction[2];
	}
	else
	{
		pzmax = ( min[2] - ray_origin[2] ) / ray_direction[2];
		pzmin = ( max[2] - ray_origin[2] ) / ray_direction[2];
	}

	if ( pmin > pzmax || pzmin > pmax )
		return 0;

	if ( pzmin > pmin )
		pmin = pzmin;

	if ( pzmax < pmax )
		pmax = pzmax;

	if ( pmin < 1e-10 )
		return 0;

	return pmin;

}

math3d::vector Box::calculate_normal( math3d::point const & point ) {

	double limit = 1e-10;


	if(abs(point[0] - this->get_min_point()[0]) < limit)
		return math3d::vector(-1,0,0);

	else if(abs(point[0] - this->get_max_point()[0]) < limit)
		return math3d::vector(1,0,0);

	else if(abs(point[1] - this->get_min_point()[1]) < limit)
		return math3d::vector(0,-1,0);

	else if(abs(point[1] - this->get_max_point()[1]) < limit)
		return math3d::vector(0,1,0);

	else if(abs(point[2] - this->get_min_point()[2]) < limit)
		return math3d::vector(0,0,1);

	else if(abs(point[2] - this->get_max_point()[2]) < limit)
		return math3d::vector(0,0,-1);

}
