/*
 * Material.cpp
 *
 *  Created on: Aug 1, 2013
 *      Author: nomve
 */

#include "Material.hpp"
#include <string>


Material::Material( std::string const & name, Color const & ka, Color const & kd, Color const & ks, float m )
	: name(name), ka(ka), kd(kd), ks(ks), m(m) {}

Material::~Material() {}

std::string Material::get_name() const {
	return name;
}

Color Material::get_ka() const {
	return ka;
}

Color Material::get_kd() const {
	return kd;
}

Color Material::get_ks() const {
	return ks;
}

double Material::get_m() const {
	return m;
}
