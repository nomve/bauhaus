/*
 * Camera.cpp
 *
 *  Created on: Aug 6, 2013
 *      Author: nomve
 */

#include "Camera.hpp"
#include "vector.hpp"
#include <cmath>

Camera::Camera( std::string name, double fov_x )
	: name(name), fov_x(fov_x), position( math3d::point(0,0,0) )
{
	/*
	 * calculate distance to the screen
	 */
	double fov_radians = fov_x * M_PI / 180;
	/*
	 * 0.5 (screen distance is 1)
	 * divided with
	 * tan of the half angle (fov_x or fov_radians)
	 * it will be multiplied with 'screen' width later
	 */
	distance = 0.5 / tan(fov_radians / 2);

}

Camera::~Camera() {}

std::string Camera::get_name() const {
	return name;
}

double Camera::get_z() const {
	return distance;
}

math3d::point Camera::get_position() const {
	return position;
}

/*
 * Ray calculation from the camera
 */
Ray Camera::fire_ray( double x, double y, double width, double height) const {

	/*
	 * vector telling us the current x, -y value of the image
	 */
	math3d::vector current(x, y, 0);

	/*
	 * substract half of the screen width from x
	 * add half of the height from y
	 * multiply distance factor with screen width
	 */
	math3d::vector correction(width/2, height/2 , distance * width);

	/*
	 * ray direction vector
	 */
	math3d::vector direction = current - correction;

	/*
	 * position is the camera position
	 */
	return Ray( position, direction, 1 );

}
