/*
 * SDF.cpp
 *
 *  Created on: Aug 2, 2013
 *      Author: nomve
 */

#include "SDF.hpp"
#include <string>
#include <sstream>
#include <iostream>
#include <cmath>
#include "color.hpp"
#include "point.hpp"
#include "matrix.hpp"
#include "Sphere.hpp"
#include "Box.hpp"

SDF::SDF() {}
SDF::SDF( std::string filename )
			:filename(filename)
{
	open_file();
};

SDF::~SDF() {}

/*
 * set path and file name
 */
void SDF::set_file( std::string name ) {
	filename = name;
}

/*
 * open the file
 */
void SDF::open_file() {
	if ( filename.length() > 0 )
		file.open( filename );
}

/*
 * parsing the file
 */
void SDF::parse_scene( Scene & scene ) {

	if ( file.is_open() )
	{
		//go through all the lines in the file
		std::string line;

		while ( std::getline(file, line) )
		{
			std::istringstream linestream( line );

			//go through all the words
			std::string word;
			std::string name;

			while ( linestream >> word )
			{
				//object definitions
				//camera, material, 3d objects...
				if ( word == "define" )
				{
					linestream >> word;

					if ( word == "material" )
					{
						//material vars
						float r, g, b, m = 0;

						//name
						linestream >> name;
						//ka
						linestream >> r >> g >> b;
						Color ka(r,g,b);
						//kd
						linestream >> r >> g >> b;
						Color kd(r,g,b);
						//ks
						linestream >> r >> g >> b;
						Color ks(r,g,b);
						//m
						linestream >> m;

						//save material
						scene.insert_material(
							std::make_shared<Material>( name, ka, kd, ks, m )
						);

					}

					else if ( word == "shape")
					{
						linestream >> word;

						double x, y, z = 0;

						//name
						linestream >> name;
						std::string material_name;

						if ( word == "sphere" )
						{
							double radius = 0;
							//point and radius
							linestream >> x >> y >> z >> radius;
							math3d::point point(x, y, z);
							//material
							linestream >> material_name;
							auto material = scene.find_material(material_name);

							std::shared_ptr<Object> sphere = std::make_shared<Sphere>(name, material, radius, point);

							//sphere
							scene.insert_object( sphere );

						}

						else if ( word == "box" )
						{
							//p1
							linestream >> x >> y >> z;
							math3d::point p1(x, y, z);
							//p2
							linestream >> x >> y >> z;
							math3d::point p2(x, y, z);
							//material
							linestream >> material_name;
							auto material = scene.find_material(material_name);

							std::shared_ptr<Object> box = std::make_shared<Box>(name, material, p1, p2);

							//box
							scene.insert_object( box );
						}

					}

					else if ( word == "light" )
					{

						linestream >> name;

						float r, g, b = 0;
						double x, y, z = 0;

						linestream >> x >> y >> z >> r >> g >> b;

						math3d::point position(x, y, z);
						Color la(r, g, b);

						linestream >> r >> g >> b;

						Color ld(r, g, b);


						scene.insert_light(
							std::make_shared<Light>( name, position, la, ld )
						);

					}

					else if ( word == "camera" )
					{

						linestream >> name;

						float fov_x = 0;
						linestream >> fov_x;


						scene.insert_camera(
							std::make_shared<Camera>( name, fov_x )
						);

					}

				}

				/*
				 * renderer options
				 */
				else if ( word == "render" )
				{
					//camera name
					std::string camera_name;
					linestream >> camera_name;

					auto camera = scene.find_camera(camera_name);
					scene.set_used_camera( camera );

					//filename to save
					std::string save_name;
					//width height
					std::size_t width, height;
					linestream >> save_name >> width >> height;
					//save name, width, height
					scene.set_scene_details( save_name, width, height );
				}

				/*
				 * transformations
				 */
				else if ( word == "transform" )
				{
					//incoming transform
					std::string transform_name;
					math3d::matrix new_transform = math3d::matrix::identity();

					//current object name
					std::string object_name;
					linestream >> object_name;

					linestream >> transform_name;

					if ( transform_name == "scale" )
					{
						double sx, sy, sz = 0.0;

						linestream >> sx >> sy >> sz;

						new_transform[0] = sx;
						new_transform[5] = sy;
						new_transform[10] = sz;

					}
					else if ( transform_name == "translate" )
					{
						double vx, vy, vz = 0.0;

						linestream >> vx >> vy >> vz;

						new_transform[12] = vx;
						new_transform[13] = vy;
						new_transform[14] = vz;


					}
					else
					{

						double angle = 0;
						linestream >> angle;
						angle = angle * M_PI / 180.0;

						double sinus = sin( angle );
						double cosinus = cos( angle );

						if ( transform_name == "rotatex" )
						{

							new_transform[5] = cosinus;
							new_transform[6] = sinus;
							new_transform[9] = -sinus;
							new_transform[10] = cosinus;

						}
						else if ( transform_name == "rotatey" )
						{

							new_transform[0] = cosinus;
							new_transform[2] = -sinus;
							new_transform[8] = sinus;
							new_transform[10] = cosinus;

						}
						else if ( transform_name == "rotatez" )
						{

							new_transform[0] = cosinus;
							new_transform[1] = sinus;
							new_transform[4] = -sinus;
							new_transform[5] = cosinus;

						}
						else
							continue;

					}

					auto object = scene.find_object( object_name );
					object->set_transformation( new_transform );

//					std::cout << object->transformation() << std::endl;
//					std::cout << object->transformation_inv() << std::endl;
//					std::cout << "----" << std::endl;

				}

			}
		}
	}
}
